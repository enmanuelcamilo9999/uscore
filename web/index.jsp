<!DOCTYPE html>
<html lang="en">
    <% String root = request.getContextPath();%>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximu-scale=1, minimum-scale=1">
        <link href="<%=root%>/css/style.css" rel="stylesheet">
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    </head>

    <body>


        <div class="user_type">
            <div class="select_type">
                <form name="loginStudent" action="<%=root%>/jsp/teacher/loginTeacher.jsp" method="POST" class="form_login">
                    <input type="submit" value="Soy docente" name="student" class="btn_teacher" />
                </form>
                <form name="loginStudent" action="<%=root%>/jsp/student/loginStudent.jsp" method="POST" class="form_login">
                    <input type="submit" value="Soy estudiante" name="student" class="btn_student" />
                </form>
            </div>
            <div class="div_img_students"><img class="img_students" src="<%=root%>/imgs/student-umet-machala.png" alt=""></div>
        </div>
        <div class="google_login"></div>
        <div class="workspace">
            <h1 class="uscore">UScore <h3 style="position:absolute;top:0;right:50px; "><a href="<%=root%>/jsp/loginAdmin.jsp" style="color:rgb(242, 103, 84); text-decoration: none;">Admin</a></h3></h1>
        </div>

        <footer>
            UFPS Cucuta <br> 2019 © All Rights Reserved. <br> Visualización de calificaciones en la Universidad

            <script>
                function signOut() {
                    var auth2 = gapi.auth2.getAuthInstance();
                    auth2.signOut().then(function () {
                    });
                    auth2.disconnect();
                }

                function deleteAllCookies() {
                    document.cookie.split(";").forEach(function (c) {
                        document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
                    });
                }
            </script>
        </footer>


    </body>

</html>