<%-- 
    Document   : studentSelectSubject
    Created on : 17/10/2019, 03:33:20 PM
    Author     : EN
--%>

<%@page import="model.Subject"%>
<%@page import="services.HtmlCreator"%>
<%@page import="java.util.List"%>
<%@page import="model.QueryManager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        String email = request.getSession().getAttribute("email").toString();
        request.getSession().setAttribute("email", email);

        QueryManager qm = new QueryManager();
        List<Subject> mySubjects = qm.getStudentSubjects(email);
        String listado = HtmlCreator.getStudentSubjects(mySubjects, request.getContextPath());

        String root = request.getContextPath();

    %>
    <head>
        <link href="<%=root%>/css/student/studentIndex.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="google_login">
            <div class="nombreEstudiante"><h3><%=email%></h3></div>
            <h3><a class="logout" href="<%=root%>/index.jsp" onclick="signOut();">Cerrar sesión</a></h3>
            <script>
                function signOut() {
                    var auth2 = gapi.auth2.getAuthInstance();
                    auth2.signOut().then(function () {
                    });
                    auth2.disconnect();
                }

                function deleteAllCookies() {
                    document.cookie.split(";").forEach(function (c) {
                        document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
                    });
                }
            </script>

        </div>
        <div class="workspace">
            <h1 class="uscore">UScore<div class="tipo">Estudiante</div></h1>
            <h3>Selecciona tu materia</h3>
            <div class="studentSpace">

                <%=listado%>


            </div>

        </div>
        <footer>
            UFPS Cucuta <br> 2019 © All Rights Reserved. <br> Visualizacion de calificaciones en la Universidad
        </footer>
    </body>
</html>
