<%-- 
    Document   : studentSubject
    Created on : 19/09/2019, 03:54:09 PM
    Author     : EN
--%>

<%@page import="services.Downloader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.File"%>
<%@page import="services.CSVController"%>
<%@page import="model.*"%>
<%@page import="services.HtmlCreator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%

            String email = request.getSession().getAttribute("email").toString();
            request.getSession().setAttribute("email", email);

            //recuperar nombre materia
            String subjectName = request.getSession().getAttribute("subjectName").toString();
            request.getSession().setAttribute("subjectName", subjectName);
            String table = "";
            String observa="";
            
            QueryManager qm = new QueryManager();
            List<Subject> subjects = qm.getStudentSubjects(email);
            String docente = "";
            Subject mySubject = null;
            for (int i = 0; i < subjects.size(); i++) {
                if (subjects.get(i).getName().equals(subjectName)) {
                    mySubject = subjects.get(i);
                    docente = subjects.get(i).getTeacher().getEmail();
                }
            }

            //Descargar archivo
            String ruta = Downloader.DEFAULT_PATH + "UScore";
            Downloader.download(mySubject.getUrlCSV(), ruta, "notas.csv");

            File file = new File(ruta + File.separator + "notas.csv");
            if (file.exists()) {
                Student actualStudent = mySubject.getStudent(email);
                table += HtmlCreator.getStudentTable(actualStudent, docente);
                observa=actualStudent.getObservaciones();
            }
            String root = request.getContextPath();


        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>
        <link href="<%=root%>/css/student/studentSubject.css" rel="stylesheet">

        <meta name="google-signin-client_id" content="923881149215-eqacititaiolmnug083n3g59o5qsn6i6.apps.googleusercontent.com">
        <title>UScore </title>
    </head>
    <body>

        <div class="google_login">
            <div class="nombreEstudiante"><h3><%=email%></h3></div>
            <h3><a class="logout" href="<%=root%>/index.jsp" onclick="signOut();">Cerrar sesión</a></h3>
            <script>
                function signOut() {
                    var auth2 = gapi.auth2.getAuthInstance();
                    auth2.signOut().then(function () {
                    });
                    auth2.disconnect();
                }

                function deleteAllCookies() {
                    document.cookie.split(";").forEach(function (c) {
                        document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
                    });
                }
            </script>

        </div>
        <div class="workspace">
            <h1 class="uscore">UScore<div class="tipo">Estudiante</div></h1>
            <div class="studentSpace">
                <p><a class="volver" href="<%=root%>/jsp/student/studentIndex.jsp"> < Volver </a></p>
                <h2><%=subjectName%><div href="" class="edit-option"></div><div class="delete-option"></div></h2>
                <div class="student-info">
                <%=table%>
                </div>
               <%=observa%>

            </div>
        </div>   
        <footer>
            UFPS Cucuta <br> 2019 © All Rights Reserved. <br> Visualizacion de calificaciones en la Universidad
        </footer>
    </body>
</html>