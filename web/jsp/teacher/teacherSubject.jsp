
<%@page import="java.net.URL"%>
<%@page import="model.QueryManager"%>
<%@page import="services.Downloader"%>
<%@page import="services.HtmlCreator"%>
<%@page import="java.io.File"%>
<%@page import="services.CSVController"%>
<%@page import="java.util.List"%>
<%@page import="model.Student"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%  String email = request.getSession().getAttribute("email").toString();
            request.getSession().setAttribute("email", email);
            String table = "";
            //recuperar nombre materia
            String subjectName = request.getSession().getAttribute("subjectName").toString();
            request.getSession().setAttribute("subjectName", subjectName);

            //Leer el csv actualizado
            QueryManager qm = new QueryManager();
            String url = qm.getUrlCSV(email, subjectName);
            String targetFolder = Downloader.DEFAULT_PATH + "UScore";
            String fileName = "notas.csv";
            Downloader.download(new URL(url), targetFolder, fileName);
            //crear la tabla
            File file = new File(targetFolder + File.separator + fileName);
            if (file.exists()) {
                List<Student> students = CSVController.getStudents(file);
                table += HtmlCreator.getStudentsTable(students);
            }
            
            String root = request.getContextPath();
            

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="google-signin-client_id" content="923881149215-eqacititaiolmnug083n3g59o5qsn6i6.apps.googleusercontent.com">
        <link href="<%=root%>/css/teacher/teacherSubject.css" rel="stylesheet">
        <title><%=subjectName%> </title>
        <script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>

    </head>
    <body>

        <div class="google_login">
            <div class="nombreEstudiante"><h3><%=email%></h3></div>
            <h3><a class="logout" href="<%=root%>/index.jsp" onclick="signOut();">Cerrar sesión</a></h3>
            <script>
                function signOut() {
                    var auth2 = gapi.auth2.getAuthInstance();
                    auth2.signOut().then(function () {
                    });
                    auth2.disconnect();
                }
                
                function deleteAllCookies() {
                    document.cookie.split(";").forEach(function (c) {
                        document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
                    });
                }
            </script>

        </div>
        <div class="workspace">
            <h1 class="uscore">UScore <div class="tipo">Docente</div></h1>
            <p><a href="<%=root%>/jsp/teacher/teacherIndex.jsp"> < Volver</a></p>
            <h3 class="subject-name"><%=subjectName%>

                <a class="edit" href="<%=root%>/jsp/teacher/teacherEditSubject.jsp">Editar</a>
                <a class="delete" onclick="borrar();">Borrar</a>
                <script>
                    function borrar() {
                        var opcion = confirm("Realmente desea borrar <%=subjectName%>");
                        if (opcion) {
                            location.href="<%=root%>/DeleteSubject.do";
                        }
                    }
                </script>
            </h3>
            <div class="theTable"><%=table%></div>

        </div>
        <footer>
            UFPS Cucuta <br> 2019 © All Rights Reserved. <br> Visualizacion de calificaciones en la Universidad
        </footer>
    </body>
</html>
