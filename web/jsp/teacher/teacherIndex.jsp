<%-- 
    Document   : teacherSelectSubject
    Created on : 17/10/2019, 02:14:22 PM
    Author     : EN
--%>

<%@page import="services.HtmlCreator"%>
<%@page import="model.*"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        String email = request.getSession().getAttribute("email").toString();
        request.getSession().setAttribute("email", email);

        QueryManager qm = new QueryManager();
        List<Subject> mySubjects = qm.getTeacherSubjects(email);
        String listado = HtmlCreator.getTeacherSubjects(mySubjects, request.getContextPath());
        String root = request.getContextPath();


    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="<%=root%>/css/teacher/teacherIndex.css" rel="stylesheet">
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

    </head>
    <body>
        <div class="google_login">
            <div class="nombreEstudiante"><h3><%=email%></h3></div>
            <h3><a class="logout" href="<%=root%>/index.jsp" onclick="signOut();">Cerrar sesión</a></h3>
            <script>
                function signOut() {
                    var auth2 = gapi.auth2.getAuthInstance();
                    auth2.signOut().then(function () {
                    });
                    auth2.disconnect();
                }

                function deleteAllCookies() {
                    document.cookie.split(";").forEach(function (c) {
                        document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
                    });
                }
            </script>

        </div>
        <div class="workspace">
            <h1 class="uscore">UScore <div class="tipo">Docente</div></h1>


            <form class="newSubject"name="createSubject" action="<%=root%>/CreateSubject.do" method="POST">
                <p>Nombre materia<input type="text" name="subjectName" value="" /></p>
                <p>Url a archivo csv<input type="text" name="urlCSV" value="" /></p>
                <input class="submitNew" type="submit" value="Crear materia" name="Crear materia" />
            </form>
            <h3>Selecciona tu materia</h3>
            <div class="studentSpace">
                <%=listado%>
            </div>
        </div>
        <footer>
            UFPS Cucuta <br> 2019 © All Rights Reserved. <br> Visualizacion de calificaciones en la Universidad
        </footer>
    </body>
</html>
