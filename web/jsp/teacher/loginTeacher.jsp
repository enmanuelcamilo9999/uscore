<!DOCTYPE html>
<html lang="en">
<% String root = request.getContextPath(); %>
<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<%=root%>/css/loginStudent.css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UScore</title>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id" content="923881149215-eqacititaiolmnug083n3g59o5qsn6i6.apps.googleusercontent.com">
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
</head>

<body>
    <div class="user_type">
        <p><a href="<%=root%>/index.jsp">No soy un docente</a></p>
    </div>
    <div class="google_login">
        <h3>Hola Docente, ingresa con Google</h3>
        <div class="g-signin2" data-onsuccess="onSignIn" id="myP"></div>
        <form name="data" action="<%=root%>/LoginTeacher.do" method="POST" style="display:none" id="formulario">
            <input type="hidden" name="email" id="emaill" />
            <input type="hidden" name="nombre" id="nombree" />
            <input type="submit" value="submit" id="subas" />
        </form>
    </div>
    <div class="workspace">
        <h1 class="uscore">UScore</h1>
    </div>
    <script src="<%=root%>/js/logT.js" type="text/javascript">
    </script>
    <footer>
        UFPS C�cuta <br> 2019 � All Rights Reserved. <br> Visualizaci�n de calificaciones en la Universidad

    </footer>

</body>

</html>