<%@page import="java.net.URL"%>
<%@page import="model.QueryManager"%>
<%@page import="services.Downloader"%>
<%@page import="services.HtmlCreator"%>
<%@page import="java.io.File"%>
<%@page import="services.CSVController"%>
<%@page import="java.util.List"%>
<%@page import="model.Student"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%  String email = request.getSession().getAttribute("email").toString();
            request.getSession().setAttribute("email", email);
            String root = request.getContextPath();


        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="google-signin-client_id" content="923881149215-eqacititaiolmnug083n3g59o5qsn6i6.apps.googleusercontent.com">
        <link href="<%=root%>/css/teacher/teacherSubject.css" rel="stylesheet">
        <title>Admin </title>
        <script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>

    </head>
    <style>

        .workspace h3, .workspace .newSubject {
            padding: 20px;
            padding-left:50px; 
            padding-top: 50px;
        }
        .newSubject{
            margin-top: 30px;
            margin-left: 30px;
            display: flex;
            flex-wrap: wrap;
        }
        .submitNew{
            width: 100px;
            margin-left: 4px;
            margin-right: 4px;
            border-radius: 5px;
            background-color: rgb(242, 103, 84);
            color: white;
            cursor:pointer;
        }
        .newSubject p input{
            width: 300px;
            margin-left: 4px;
            margin-right: 4px;
            border: 2px solid gray;
            border-radius: 5px;
        }

    </style>
    <body>

        <div class="google_login">
            <div class="nombreEstudiante"><h3><%=email%></h3></div>
            <h3><a class="logout" href="<%=root%>/index.jsp" onclick="signOut();">Cerrar sesión</a></h3>
            <script>
                function signOut() {
                    var auth2 = gapi.auth2.getAuthInstance();
                    auth2.signOut().then(function () {
                    });
                    auth2.disconnect();
                }

                function deleteAllCookies() {
                    document.cookie.split(";").forEach(function (c) {
                        document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
                    });
                }
            </script>

        </div>
        <div class="workspace">
            <h1 class="uscore">UScore <div class="tipo">Admin</div></h1>

            <form class="newSubject"name="createSubject" action="<%=root%>/RegisterTeacher.do" method="POST">
                <p>Nombre del docente<input type="text" name="name" value="" /></p>
                <p>Correo del docente<input type="text" name="email" value="" /></p>
                <input class="submitNew" type="submit" value="Agregar docente" name="add docente" />
            </form>



        </div>
        <footer>
            UFPS Cucuta <br> 2019 © All Rights Reserved. <br> Visualizacion de calificaciones en la Universidad
        </footer>
    </body>
</html>