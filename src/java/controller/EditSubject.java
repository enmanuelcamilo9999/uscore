/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.QueryManager;

/**
 *
 * @author EN
 */
public class EditSubject extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String email = request.getSession().getAttribute("email").toString();
            request.getSession().setAttribute("email", email);
            String nombreActual = request.getSession().getAttribute("subjectName").toString();
            request.getSession().setAttribute("subjectName", nombreActual);

            String nombre = request.getParameter("subjectName");
            String url = request.getParameter("urlCSV");

            QueryManager qm = new QueryManager();
            boolean nombreExito=qm.editarNombreMateria(nombreActual, email, nombre);
            boolean urlExito=qm.editarUrlMateria(nombreActual, email, url);
            System.out.println("Se pudo editar el nombre de la materia:"+nombreExito);
             System.out.println("Se pudo editar el url de la materia:"+urlExito);
            response.sendRedirect(request.getContextPath() + "/jsp/teacher/teacherIndex.jsp");
        } catch (Exception e) {
            response.sendRedirect(request.getContextPath() + "/jsp/teacher/teacherIndex.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
