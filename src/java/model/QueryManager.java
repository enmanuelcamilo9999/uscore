/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.HeadlessException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import services.CSVController;
import services.Downloader;

/**
 *
 * @author EN
 */
public class QueryManager extends Connector {

    public boolean loginAdmin(String email) {
        boolean exist = false;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConecction();
        String sql = "SELECT * FROM admin WHERE email=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, email);
            rs = ps.executeQuery();

            if (rs.next()) {
                if (rs.getString("email").equals(email)) {
                    exist = true;
                }

            }

        } catch (SQLException e) {
            System.err.println(e);

        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }

        }
        return exist;
    }

    public boolean registrarDocente(String email, String nombre) {
        boolean wasLogged = false;
        PreparedStatement ps = null;
        java.sql.Connection con = getConecction();

        String sql = "INSERT INTO docente(email,nombre) VALUES (?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, nombre);
            ps.executeUpdate();
            wasLogged = true;
        } catch (HeadlessException | SQLException e) {
            System.err.println(e);

        } finally {
            try {
                ps = null;
                con.close();
                wasLogged = true;
            } catch (SQLException e) {
                System.out.println("Error al cerrar la conexion " + e);
            }

            if (con == null) {
                System.out.println("Conexion terminada...");
            }
        }
        return wasLogged;
    }

    public boolean existeDocente(String email) {
        boolean exist = false;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConecction();
        String sql = "SELECT * FROM docente WHERE email=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, email);
            rs = ps.executeQuery();

            if (rs.next()) {
                exist = true;

            }

        } catch (SQLException e) {
            System.err.println(e);

        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }

        }
        return exist;
    }

    public List<Subject> getTeacherSubjects(String emailTeacher) throws MalformedURLException {

        List<Subject> subjects = new ArrayList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConecction();
        String sql = "SELECT * FROM asignatura WHERE email_doc=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, emailTeacher);

            rs = ps.executeQuery();

            while (rs.next()) {
                String nombre = rs.getString("nombre_asig");
                Teacher maestro = new Teacher(rs.getString("email_doc"));
                String url = rs.getString("urlCSV");
                Subject actual = new Subject(nombre, maestro, new URL(url));
                subjects.add(actual);

            }
            con.close();
        } catch (SQLException e) {
            System.err.println(e);
            return subjects;

        }

        return subjects;
    }

    public String getUrlCSV(String emailTeacher, String subjectName) throws MalformedURLException {

        String urlCSV = null;

        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConecction();
        String sql = "SELECT * FROM asignatura WHERE email_doc=? AND nombre_asig=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, emailTeacher);
            ps.setString(2, subjectName);

            rs = ps.executeQuery();

            while (rs.next()) {
                urlCSV = rs.getString("urlCSV");
            }

        } catch (SQLException e) {
            System.err.println(e);

        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }

        }

        return urlCSV;
    }

    public List<Subject> getAllSubjects() throws MalformedURLException {

        List<Subject> subjects = new ArrayList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConecction();
        String sql = "SELECT * FROM asignatura ";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                String nombre = rs.getString("nombre_asig");
                Teacher maestro = new Teacher(rs.getString("email_doc"));
                String url = rs.getString("urlCSV");
                Subject actual = new Subject(nombre, maestro, new URL(url));
                subjects.add(actual);

            }

        } catch (SQLException e) {
            System.err.println(e);

        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }

        }

        return subjects;
    }

    // es usado en studentIndex.jsp
    public List<Subject> getStudentSubjects(String email) throws MalformedURLException {
        List<Subject> subjects = new ArrayList<>();
        List<Subject> allSubjects = getAllSubjects();

        String path = Downloader.DEFAULT_PATH + "Uscore";
        for (int i = 0; i < allSubjects.size(); i++) {
            Subject actualSub = allSubjects.get(i);

            //descargar archivo csv
            String fileName = "notas.csv";
            File file = new File(path + File.separator + fileName);
            Downloader.download(actualSub.getUrlCSV(), path, fileName);

            //obtener objetos estudiante de csv   
            List<Student> students = CSVController.getStudents(file);

            //asignarlo a un objeto subject
            Subject mySubject = new Subject(actualSub.getName(), actualSub.getTeacher(), actualSub.getUrlCSV(), students);

            //validar si existe el estudiante en cada csv
            if (mySubject.existStudent(email)) {
                subjects.add(mySubject);
            }

        }

        return subjects;
    }

    public boolean registrarMateria(String emailDocente, String urlCSV, String nombreMateria) {
        boolean wasLogged = false;

        if (!existeMateria(nombreMateria, emailDocente)) {

            PreparedStatement ps = null;
            java.sql.Connection con = getConecction();

            String sql = "INSERT INTO asignatura(email_doc,nombre_asig,urlCSV) VALUES (?,?,?)";
            try {
                ps = con.prepareStatement(sql);
                ps.setString(1, emailDocente);
                ps.setString(2, nombreMateria);
                ps.setString(3, urlCSV);
                ps.executeUpdate();
                wasLogged = true;
            } catch (HeadlessException | SQLException e) {
                System.err.println(e);

            } finally {
                try {
                    ps = null;
                    con.close();
                    wasLogged = true;
                } catch (SQLException e) {
                    System.out.println("Error al cerrar la conexion " + e);
                }

                if (con == null) {
                    System.out.println("Conexion terminada...");
                }
            }

        }
        return wasLogged;
    }

    public boolean editarNombreMateria(String nombreMateriaActual, String emailDocente, String nombreMateriaNuevo) {
        boolean correcto = false;

        if (!existeMateria(nombreMateriaNuevo, emailDocente)) {
            String sql = "UPDATE asignatura SET nombre_asig=? WHERE email_doc=? AND nombre_asig=? ";
            PreparedStatement ps = null;
            java.sql.Connection con = getConecction();
            try {
                ps = con.prepareStatement(sql);
                ps.setString(1, nombreMateriaNuevo);
                ps.setString(2, emailDocente);
                ps.setString(3, nombreMateriaActual);
                ps.executeUpdate();
            } catch (HeadlessException | SQLException e) {
                System.err.println(e);

            } finally {
                try {
                    ps = null;
                    con.close();
                    correcto = true;
                } catch (SQLException e) {
                    System.out.println("Error al cerrar la conexion " + e);
                }

                if (con == null) {
                    System.out.println("Conexion terminada...");
                }
            }
        }

        return correcto;
    }

    public boolean editarUrlMateria(String nombreMateriaActual, String emailDocente, String urlNuevo) {
        boolean correcto = false;
        PreparedStatement ps = null;
        java.sql.Connection con = getConecction();

        String sql = "UPDATE asignatura SET urlCSV=? WHERE email_doc=? AND nombre_asig=? ";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, urlNuevo);
            ps.setString(2, emailDocente);
            ps.setString(3, nombreMateriaActual);
            ps.executeUpdate();
        } catch (HeadlessException | SQLException e) {
            System.err.println(e);

        } finally {
            try {
                ps = null;
                con.close();
                correcto = true;
            } catch (SQLException e) {
                System.out.println("Error al cerrar la conexion " + e);
            }

            if (con == null) {
                System.out.println("Conexion terminada...");
            }
        }
        return correcto;
    }

    public boolean borrarMateria(String nombreMateriaActual, String emailDocente) {
        boolean correcto = false;
        PreparedStatement ps = null;
        java.sql.Connection con = getConecction();

        String sql = "DELETE FROM asignatura  WHERE email_doc=? AND nombre_asig=? ";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, emailDocente);
            ps.setString(2, nombreMateriaActual);
            ps.executeUpdate();
        } catch (HeadlessException | SQLException e) {
            System.err.println(e);

        } finally {
            try {
                ps = null;
                con.close();
                correcto = true;
            } catch (SQLException e) {
                System.out.println("Error al cerrar la conexion " + e);
            }

            if (con == null) {
                System.out.println("Conexion terminada...");
            }
        }
        return correcto;
    }

    public boolean existeMateria(String nombreMateriaActual, String emailDocente) {
        boolean correcto = false;
        PreparedStatement ps = null;
        java.sql.Connection con = getConecction();

        String sql = "SELECT nombre_asig FROM asignatura  WHERE email_doc=? AND nombre_asig=? ";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, emailDocente);
            ps.setString(2, nombreMateriaActual);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                correcto = true;
            }

        } catch (HeadlessException | SQLException e) {
            System.err.println(e);

        } finally {
            try {
                ps = null;
                con.close();
            } catch (SQLException e) {
                System.out.println("Error al cerrar la conexion " + e);
            }

            if (con == null) {
                System.out.println("Conexion terminada...");
            }
        }
        return correcto;
    }

    public static void main(String[] args) throws MalformedURLException {
        QueryManager qm = new QueryManager();

        /*List<Subject> mySubjects = qm.getStudentSubjects("enmanuelcamilo9999@gmail.com");
        System.out.println(mySubjects.get(1).getName());
         */
        boolean c = qm.existeMateria("Sistemas Operativosa", "enmanuelcamilomr@ufps.edu.co");
        System.out.println("existe: " + c);

    }

}
