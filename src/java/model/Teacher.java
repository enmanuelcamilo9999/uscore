
package model;

import java.util.List;

/**
 *
 * @author EN
 */
public class Teacher {
    private String email;
    private String name;
    public List<Subject> subjects;

    public Teacher(String email, String name, List<Subject> subjects) {
        this.email = email;
        this.name = name;
        this.subjects = subjects;
    }

    public Teacher(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public Teacher(String email) {
        this.email = email;
    }
    

    public Teacher() {
    }

    
    

  public boolean login(String email){
      
      boolean wasLogged=false;
      
      
      
      
      return wasLogged;
  
     
  }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }


}

