package model;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author EN
 */
public class Subject {

    private String name;
    private Teacher teacher;
    private URL urlCSV;
    String observaciones;
    private List<Student> students;

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Subject(String name, Teacher teacher, URL urlCSV, List<Student> students) {
        this.name = name;
        this.teacher = teacher;
        this.urlCSV = urlCSV;
        this.students = students;
    }

    public Subject(String name, Teacher teacher, URL urlCSV) {
        this(name, teacher, urlCSV, null);
    }

    public Subject(String name, URL urlCSV, List<Student> students) {
        this.name = name;
        this.urlCSV = urlCSV;
        this.students = students;
    }

    public List<String> getQualificationsConcepts() {
        return students.get(0).getQualificationsConcepts();

    }

    public Student getStudent(String email) {
        Student s = null;
        byte quant = (byte) students.size();
        for (int i = 0; i < quant; i++) {
            Student stu = students.get(i);
            if (stu.getEmail().equals(email)) {
                s = stu;
            }
        }
        return s;
    }

    public boolean existStudent(String email) {
        boolean exist = false;
        for (Student student : students) {
            if (student.getEmail().equals(email)) {
                exist = true;
            }
        }
        return exist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public URL getUrlCSV() {
        return urlCSV;
    }

    public void setUrlCSV(URL urlCSV) {
        this.urlCSV = urlCSV;
    }

}
