package services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import model.Subject;

/**
 * @author chuidiang Moddified by:EN
 *
 */
public class Downloader {

    /**
     * Descarga un fichero jpeg y lo guarda en e:/foto.jpg
     *
     */
    public static final String DEFAULT_PATH =System.getProperty("java.io.tmpdir");

    public static void download(URL url, String targetFolder, String fileName) {
        try {
            //Crear la carpeta destino
            File dir= new File(targetFolder);
            dir.mkdir();
            //Borrar un archivo viejo con ese nombre
            File oldFile= new File(targetFolder+File.separator+fileName);
            oldFile.delete();
            // establecemos conexion
            URLConnection urlCon = url.openConnection();

            // Sacamos por pantalla el tipo de fichero
            System.out.println(urlCon.getContentType());

            // Se obtiene el inputStream de la foto web y se abre el fichero
            // local.
            InputStream is = urlCon.getInputStream();
            FileOutputStream fos = new FileOutputStream(targetFolder + File.separator + fileName);

            // Lectura de la foto de la web y escritura en fichero local
            byte[] array = new byte[1000]; // buffer temporal de lectura.
            int leido = is.read(array);
            while (leido > 0) {
                fos.write(array, 0, leido);
                leido = is.read(array);
            }

            // cierre de conexion y fichero.
            is.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        
    }

}
